<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $table = "tasks";

    /**
     * Relationship task table with user table
     * 
     */
    public function users()
    {
        return $this->belongsToMany('users', 'user_tasks');
    }

    /**
     * Relationship task table with project table
     * 
     */
    public function project()
    {
        return $this->belongsTo('projects', 'project_id');
    }
}
