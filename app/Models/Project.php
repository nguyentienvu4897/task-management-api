<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $table = "projects";

    /**
     * Relationship project table with user table
     * 
     */
    public function users()
    {
        return $this->belongsToMany('users', 'user_projects');
    }

    /**
     * Relationship project table with task table
     * 
     */
    public function tasks()
    {
        return $this->hasMany('tasks', 'project_id');
    }
}
